package com.joocy.tests

import java.util.*

class NameFormatter(private val pattern: String) {

    fun format(vararg names: String): String {
        var nameArgs = names.clone()
        while(true) {
            try {
                return pattern.format(*nameArgs)
            } catch (exception: MissingFormatArgumentException) {
                nameArgs = arrayOf(*nameArgs, "%s")
            }
        }
    }

}
