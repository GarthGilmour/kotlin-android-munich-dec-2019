package com.joocy.tests

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_name.*

class NameActivity : AppCompatActivity() {

    companion object {
        val REQUEST_CODE = 20001
        val EXTRA_NAME = "name"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_name)
    }

    fun onClick(view: View) {
        val name = nameEdit.text.toString()
        if (name.isBlank()) {
            setResult(Activity.RESULT_CANCELED)
        } else {
            val intent = Intent().apply {
                putExtra(EXTRA_NAME, name)
            }
            setResult(Activity.RESULT_OK, intent)
        }
        finish()
    }
}
