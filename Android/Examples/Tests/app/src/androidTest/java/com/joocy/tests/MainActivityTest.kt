package com.joocy.tests

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.typeText
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import org.junit.Rule
import org.junit.Test

class MainActivityTest {

    @Rule
    @JvmField
    var rule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun clicking_on_name_button_should_start_name_activity() {
        clickGetNameButton()
        onView(withText(R.string.name_label)).check(matches(isDisplayed()))
    }

    @Test
    fun submitted_name_should_display_on_main_activity() {
        clickGetNameButton()
        onView(withId(R.id.nameEdit)).perform(typeText("Tester"))
        clickOkButton()
        onView(withId(R.id.nameText)).check(matches(withText("Well, hello there Tester!")))
    }

    @Test
    fun submitting_an_empty_name_should_display_stranger_message() {
        clickGetNameButton()
        clickOkButton()
        onView(withId(R.id.nameText)).check(matches(withText("Howdy stranger!")))
    }

    private fun clickOkButton() {
        onView(withText(R.string.ok)).perform(click())
    }

    private fun clickGetNameButton() {
        onView(withId(R.id.nameButton)).perform(click())
    }

}
