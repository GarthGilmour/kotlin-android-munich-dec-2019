package com.joocy.services

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder

class MessageService : Service() {

    companion object {
        val MESSAGES = listOf("Waiting...", "Hang on...", "I'm getting it now...", "Are you in a hurry?")
    }

    private val binder = MessageBinder()

    override fun onBind(intent: Intent): IBinder = binder

    fun getMessage(): String = MESSAGES.random()

    inner class MessageBinder: Binder() {
        val messageService: MessageService
            get() = this@MessageService
    }
}
