package com.joocy.services

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class WeatherForecast(val location: String, val day: String, val forecast: String) : Parcelable