package com.joocy.activities

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun startActivity(view: View) {
        val intent = Intent(this, InfoActivity::class.java)
        startActivity(intent)
    }

    fun startActionActivity(view: View) {
        val intent = Intent(Intent.ACTION_DIAL).apply {
            data = Uri.parse("tel: +1 650-253-0000")
        }
        startActivity(intent)
    }

    fun startActivityForResult(view: View) {
        val intent = Intent(this, NameActivity::class.java)
        startActivityForResult(intent, NameActivity.REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == NameActivity.REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val resultName =
                if (data != null) { "Thanks ${data.getStringExtra(NameActivity.EXTRA_NAME)}" }
                else  { "No name returned"}
            resultText.text = resultName
        }
    }

}
