package co.instil.viewmodel

interface TranslationService {

    fun translateText(languagePair: LanguagePair, text: String): String

}