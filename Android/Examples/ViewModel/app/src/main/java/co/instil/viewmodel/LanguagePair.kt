package co.instil.viewmodel

data class LanguagePair (val first: String, val second: String) {
    override fun toString(): String {
        return "${first}|${second}"
    }
}