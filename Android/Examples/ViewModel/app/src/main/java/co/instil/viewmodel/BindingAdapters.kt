package co.instil.viewmodel

import android.view.View
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData

@BindingAdapter("android:visibility")
fun setVisibility(view: View, isVisible: LiveData<Boolean>) {
    isVisible.value?.let{
        view.visibility = if (it) View.VISIBLE else View.INVISIBLE
    }
}