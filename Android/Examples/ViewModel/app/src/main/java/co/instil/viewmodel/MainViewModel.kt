package co.instil.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainViewModel(val translationService: TranslationService,
                    val dispatcher: CoroutineDispatcher = Dispatchers.Default): ViewModel() {

    var languagePair = LanguagePair("en", "de")

    companion object {
        fun FACTORY(translationService: TranslationService) =
            object: ViewModelProvider.NewInstanceFactory() {
                override fun <T : ViewModel> create(modelClass: Class<T>): T {
                    return MainViewModel(translationService) as T
                }
            }
    }

    val translatedText = MutableLiveData<String>()
    val working = MutableLiveData<Boolean>(false)

    fun setLanguage(language: String) {
        languagePair = LanguagePair("en", language)
    }

    fun translateText(text: String) {
        viewModelScope.launch {
            working.postValue(true)
            val result = getTranslation(languagePair, text)
            translatedText.postValue(result)
            working.postValue(false)
        }
    }

    private suspend fun getTranslation(languagePair: LanguagePair, text: String): String =
        withContext(dispatcher){
            translationService.translateText(languagePair, text)
        }
}