package co.instil.viewmodel

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import co.instil.viewmodel.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val translationService = SlowTranslationService()
        val viewModel = ViewModelProviders
            .of(this, MainViewModel.FACTORY(translationService))
            .get(MainViewModel::class.java)

        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.lifecycleOwner = this
        binding.vm = viewModel

        viewModel.translatedText.observe(this, Observer<String> { translatedText ->
            translatedView.text = translatedText
        })

        textToTranslate.setOnKeyListener() { _, _ , _ ->
            translatedView.text = ""
            true
        }

        translateButton.setOnClickListener {
            val text = textToTranslate.text.toString()
            viewModel.translateText(text)
        }
    }
}

class SlowTranslationService: TranslationService {
    override fun translateText(languagePair: LanguagePair, text: String): String {
        Thread.sleep(Random.nextLong(1_000, 3_000))
        return text.toUpperCase()
    }
}
