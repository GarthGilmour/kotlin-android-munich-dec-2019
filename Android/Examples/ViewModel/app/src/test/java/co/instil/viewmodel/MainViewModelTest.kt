package co.instil.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

@ExperimentalCoroutinesApi
class MainViewModelTest {
    @get:Rule
    val instantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    private val dispatcher = TestCoroutineDispatcher()

    private lateinit var subject: MainViewModel
    private val mockTranslationService = mock<TranslationService>()

    @Before
    fun setup() {
        Dispatchers.setMain(dispatcher)
        subject = MainViewModel(mockTranslationService, dispatcher)
    }

    @After
    fun tearDown() {
        dispatcher.cleanupTestCoroutines()
    }

    @Test
    fun shouldCallServiceWithDefaultLanguageAndProvidedText() = dispatcher.runBlockingTest {
        val languagePair = LanguagePair("en", "de")
        val text = "translate this"

        subject.translateText(text)

        verify(mockTranslationService).translateText(languagePair, text)
    }

    @Test
    fun shouldCallServiceWithRequestedLanguageAndProvidedTest() = dispatcher.runBlockingTest {
        val languagePair = LanguagePair("en", "fr")
        val text = "translate this"

        subject.setLanguage("fr")
        subject.translateText(text)

        verify(mockTranslationService).translateText(languagePair, text)
    }

    @Test
    fun shouldSetTheTranslatedTextToTheValueReturnedByTheService() = dispatcher.runBlockingTest {
        val text = "translate text";
        val translatedText: String = text.reversed()
        whenever(mockTranslationService.translateText(any(), any())).thenReturn(translatedText)

        subject.translateText(text)

        assertThat(subject.translatedText.value, equalTo(translatedText))
    }
}