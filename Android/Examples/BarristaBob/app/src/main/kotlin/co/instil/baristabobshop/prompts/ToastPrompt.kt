/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.prompts

import android.content.Context
import android.widget.Toast

open class ToastPrompt(
    private val applicationContext: Context
) {

    open fun showMessage(message: String) = Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
}