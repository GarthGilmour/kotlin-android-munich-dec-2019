package co.instil.baristabobshop.drinksmenu.viewmodel

import android.databinding.ObservableBoolean
import android.support.v7.app.AppCompatActivity
import co.instil.baristabobshop.android.viewmodel.ActivityViewModel
import co.instil.baristabobshop.drinksmenu.periodicmenuupdates.DrinksMenuUpdater
import co.instil.baristabobshop.drinksmenu.service.DrinksMenuService
import io.reactivex.rxkotlin.subscribeBy
import org.slf4j.LoggerFactory
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DrinksMenuViewModel @Inject constructor(
    private val drinksMenuService: DrinksMenuService,
    private val drinksMenuUpdater: DrinksMenuUpdater
) : ActivityViewModel() {

    var isUpdatingDrinksMenu = ObservableBoolean(true)
    private val logger = LoggerFactory.getLogger(DrinksMenuViewModel::class.java)

    override fun onCreate(activity: AppCompatActivity) {
        super.onCreate(activity)
        updateLocalDatabaseFromApi()
    }

    fun getAllDrinks() = drinksMenuService.getAllDrinks()

    private fun updateLocalDatabaseFromApi() {
        drinksMenuService.updateDrinksMenu()
            .subscribeOn(baristaBobSchedulers.ioScheduler())
            .observeOn(baristaBobSchedulers.uiScheduler())
            .doFinally(::doAfterDrinksMenuRefresh)
            .subscribeBy(onError = { error -> logger.error("Could not update Drinks Menu", error) })
    }

    private fun doAfterDrinksMenuRefresh() {
        isUpdatingDrinksMenu.set(false)
        drinksMenuUpdater.schedulePeriodicDrinksMenuUpdate()
    }
}
