/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.checkout.viewmodel

import android.databinding.ObservableBoolean
import co.instil.baristabobshop.R
import co.instil.baristabobshop.android.viewmodel.ActivityViewModel
import co.instil.baristabobshop.checkout.model.PaymentMethod.CARD
import co.instil.baristabobshop.checkout.model.PaymentMethod.CASH
import co.instil.baristabobshop.checkout.model.PaymentMethod.NONE
import co.instil.baristabobshop.checkout.service.CheckoutService
import co.instil.baristabobshop.databinding.ObservableField
import co.instil.baristabobshop.drinksorder.DrinkOrderDetails
import co.instil.baristabobshop.navigation.NavigationRouter
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.rx2.await
import kotlinx.coroutines.rx2.rxSingle
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CheckoutViewModel @Inject constructor(
    private val checkoutService: CheckoutService,
    private val navigationRouter: NavigationRouter
) : ActivityViewModel() {

    private var checkoutItemList = listOf<DrinkOrderDetails>()

    private val scope = CoroutineScope(Dispatchers.IO)

    var confirmButtonEnabled = ObservableBoolean(false)
    var totalCost = ObservableField("")
    var cashButtonClicked = ObservableBoolean(false)
    var cardButtonClicked = ObservableBoolean(false)

    private fun getTotalCostFormatted() = resourceService.getStringResource(R.string.format_currency_gbp).format(calculateTotalCost())

    private fun calculateTotalCost() = checkoutItemList.sumByDouble { it.quantity * it.drink.cost }

    fun onRadioButtonCheckedChanged() {
        confirmButtonEnabled.set(true)
    }

    fun onCancelButtonClicked() {
        navigationRouter.navigateToDrinksOrder()
    }

    fun getDrinksOrdersAndUpdateDetails() = scope.rxSingle {
        checkoutItemList = checkoutService.getAllDrinkOrderDetails().await()
        if (checkoutItemList.isEmpty()) {
            navigationRouter.navigateToDrinksOrder()
        }
        confirmButtonEnabled.set(false)
        totalCost.set(getTotalCostFormatted())

        return@rxSingle checkoutItemList
    }

    fun onConfirmButtonClicked() {
        checkoutService.sendOrder(checkoutItemList, identifyPaymentMethod())
            .observeOn(baristaBobSchedulers.uiScheduler())
            .subscribeOn(baristaBobSchedulers.ioScheduler())
            .subscribeBy(onComplete = ::onOrderComplete)
    }

    private fun identifyPaymentMethod() = when {
        cardButtonClicked.get() -> CARD
        cashButtonClicked.get() -> CASH
        else -> NONE
    }

    private fun onOrderComplete() {
        checkoutService.checkoutPendingDrinkOrders()
            .subscribeOn(baristaBobSchedulers.ioScheduler())
            .observeOn(baristaBobSchedulers.uiScheduler())
            .subscribeBy(onComplete = ::onCheckoutComplete)
    }

    private fun onCheckoutComplete() {
        navigationRouter.navigateToDrinksOrder()
    }
}