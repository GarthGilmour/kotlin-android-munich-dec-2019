package co.instil.baristabobshop.databinding

class ObservableField<T : Any> : android.databinding.ObservableField<T> {

    /**
     * Create an observable field with default value.
     */
    constructor(value: T) : super(value)

    /**
     * Create an observable field without default value, this
     * should be used with care as a value must be set before
     * reading, similar to a lateinit var.
     */
    constructor() : super()

    override fun get(): T = super.get()!!

    @Suppress("RedundantOverride")
    override fun set(value: T) = super.set(value)
}

class OptionalObservableField<T : Any?>(value: T? = null) : android.databinding.ObservableField<T>(value)