/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.drinksorder.recyclerview.viewmodel

import co.instil.baristabobshop.R
import co.instil.baristabobshop.builder.createDrink
import co.instil.baristabobshop.drinksorder.DrinkOrderDetails
import co.instil.baristabobshop.ui.service.ResourceService
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test

class DrinkOrderDetailsViewModelUnitTest {

    private val resourceService = mock<ResourceService>()

    private val drinkOrderDetails = DrinkOrderDetails(4, 1, createDrink(name = "Coffee", cost = 3.00))

    private lateinit var target: DrinkOrderDetailsViewModel

    @Before
    fun beforeEachTest() {
        given(resourceService.getStringResource(R.string.format_currency_gbp)).willReturn("£%.2f")
        given(resourceService.getStringResource(R.string.format_two_decimal_place)).willReturn("%.2f")
        given(resourceService.getStringResource(R.string.gbp_symbol)).willReturn("£")
        given(resourceService.getStringResource(R.string.prefix_quantity)).willReturn("Quantity: ")

        target = DrinkOrderDetailsViewModel(drinkOrderDetails, resourceService)
    }

    @Test
    fun shouldReturnItemName() {
        assertThat(target.name, `is`("Coffee"))
    }

    @Test
    fun shouldFormatAndReturnCost() {
        assertThat(target.cost, `is`("£3.00"))
    }

    @Test
    fun shouldFormatAndReturnQuantity() {
        assertThat(target.quantity, `is`("Quantity: 4"))
    }

    @Test
    fun shouldReturnImageId() {
        assertThat(target.image, `is`(R.drawable.ic_coffee_cup))
    }
}