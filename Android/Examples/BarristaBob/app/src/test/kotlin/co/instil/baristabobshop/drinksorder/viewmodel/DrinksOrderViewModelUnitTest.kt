/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.drinksorder.viewmodel

import co.instil.baristabobshop.builder.createDrink
import co.instil.baristabobshop.drinksorder.DrinkOrderDetails
import co.instil.baristabobshop.drinksorder.service.DrinksOrderService
import co.instil.baristabobshop.navigation.NavigationRouter
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Single
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class DrinksOrderViewModelUnitTest {

    private val navigationRouter = mock<NavigationRouter>()
    private val drinksOrderService = mock<DrinksOrderService>()

    private val drinkOrders = listOf(
        DrinkOrderDetails(1, 1, createDrink(cost = 1.00)),
        DrinkOrderDetails(2, 2, createDrink(cost = 2.00)),
        DrinkOrderDetails(3, 3, createDrink(cost = 3.00))
    )

    val target = DrinksOrderViewModel(navigationRouter, drinksOrderService)

    @Test
    fun shouldNavigateToDrinksMenuWhenOnAddToOrderButtonClicked() {
        target.onAddToOrderButtonClicked()

        verify(navigationRouter).navigateToDrinksMenu()
    }

    @Test
    fun shouldNavigateToCheckoutMenuWhenCheckoutButtonClicked() {
        target.onCheckoutButtonClicked()

        verify(navigationRouter).navigateToCheckout()
    }

    @Test
    fun shouldCallGetAllDrinksOrderItem() {
        given(drinksOrderService.getAllDrinksOrderItems()).willReturn(Single.just(emptyList()))

        target.getDrinkOrdersAndUpdateDetails().blockingGet()

        verify(drinksOrderService).getAllDrinksOrderItems()
    }

    @Test
    fun shouldSetIsOrderListEmptyToTrue() {
        given(drinksOrderService.getAllDrinksOrderItems()).willReturn(Single.just(emptyList()))

        target.getDrinkOrdersAndUpdateDetails().blockingGet()

        assertThat(target.isOrderListEmpty.get(), `is`(true))
    }

    @Test
    fun shouldSetIsOrderListEmptyToFalse() {
        given(drinksOrderService.getAllDrinksOrderItems()).willReturn(Single.just(drinkOrders))

        target.getDrinkOrdersAndUpdateDetails().blockingGet()

        assertThat(target.isOrderListEmpty.get(), `is`(false))
    }

    @Test
    fun shouldSetFormattedCheckoutTotalWithZero() {
        given(drinksOrderService.getAllDrinksOrderItems()).willReturn(Single.just(emptyList()))

        target.getDrinkOrdersAndUpdateDetails().blockingGet()

        assertThat(target.formattedCheckoutTotal.get(), `is`("Checkout £0.00"))
    }

    @Test
    fun shouldSetFormattedCheckoutTotalWithTotalCost() {
        given(drinksOrderService.getAllDrinksOrderItems()).willReturn(Single.just(drinkOrders))

        target.getDrinkOrdersAndUpdateDetails().blockingGet()

        assertThat(target.formattedCheckoutTotal.get(), `is`("Checkout £14.00"))
    }
}