/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.builder

import co.instil.baristabobshop.R
import co.instil.baristabobshop.drink.persistence.Drink
import java.util.Date

fun createDrink(
    name: String = "TestName",
    description: String = "Test Description",
    cost: Double = 2.00,
    imageId: Int = R.drawable.ic_coffee_cup,
    timeStamp: Date = Date()
) =
    Drink(
        name,
        description,
        cost,
        imageId,
        timeStamp
    )