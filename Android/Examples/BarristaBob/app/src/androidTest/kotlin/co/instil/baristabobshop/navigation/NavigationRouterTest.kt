/*
 * Copyright 2018 Instil.
 */
package co.instil.baristabobshop.navigation

import android.app.Activity
import android.app.Activity.RESULT_OK
import android.app.Instrumentation.ActivityResult
import android.content.Intent
import android.support.test.espresso.intent.Intents
import android.support.test.espresso.intent.matcher.IntentMatchers.anyIntent
import android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent
import android.support.test.espresso.intent.matcher.IntentMatchers.hasExtra
import android.support.test.espresso.intent.matcher.IntentMatchers.hasFlag
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.runner.AndroidJUnit4
import co.instil.baristabobshop.checkout.activity.CheckoutActivity
import co.instil.baristabobshop.dagger.DaggerApplicationInitializer
import co.instil.baristabobshop.drinksmenu.activity.DrinksMenuActivity
import co.instil.baristabobshop.drinksorder.activity.DrinksOrderActivity
import co.instil.baristabobshop.login.activity.LoginActivity
import org.hamcrest.Matchers.allOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import kotlin.reflect.KClass

@RunWith(AndroidJUnit4::class)
class NavigationRouterTest {

    @Rule @JvmField val activityTestRule = IntentsTestRule(LoginActivity::class.java, true, false)
    @Rule @JvmField val daggerApplicationRule = DaggerApplicationInitializer(this)

    lateinit var target: NavigationRouter

    @Before
    fun beforeEachTest() {
        activityTestRule.launchActivity(null)

        // Do not handle any intents, only verify their contents in test
        Intents.intending(anyIntent()).respondWith(ActivityResult(RESULT_OK, Intent()))

        target = NavigationRouter(activityTestRule.activity.applicationContext)
    }

    @Test
    fun shouldNavigateToLogIn() {
        target.navigateToLogin()

        verifyActivityIntent(LoginActivity::class)
    }

    @Test
    fun shouldNavigateToDrinksOrderAndClearTask() {
        target.navigateToDrinksOrder()

        verifyActivityIntentClearTask(DrinksOrderActivity::class)
    }

    @Test
    fun shouldNavigateToDrinksMenu() {
        target.navigateToDrinksMenu()

        verifyActivityIntent(DrinksMenuActivity::class)
    }

    @Test
    fun shouldNavigateToCheckout() {
        target.navigateToCheckout()

        verifyActivityIntent(CheckoutActivity::class)
    }

    private fun <T : Activity> verifyActivityIntentClearTask(activityClass: KClass<T>, extras: Map<String, Any> = emptyMap()) {
        verifyActivityIntent(activityClass, extras, Intent.FLAG_ACTIVITY_CLEAR_TASK)
    }

    private fun <T : Activity> verifyActivityIntent(activityClass: KClass<T>, extras: Map<String, Any> = emptyMap(), flags: Int = Intent.FLAG_ACTIVITY_NEW_TASK) {
        Intents.intended(
            allOf(
                hasComponent(activityClass.java.name),
                *extras.map { entry -> hasExtra(entry.key, entry.value) }.toTypedArray(),
                hasFlag(flags)
            )
        )
    }
}