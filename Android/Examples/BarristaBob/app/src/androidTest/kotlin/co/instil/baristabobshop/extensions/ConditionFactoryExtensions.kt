package co.instil.baristabobshop.extensions

import org.awaitility.core.ConditionFactory
import java.util.concurrent.TimeUnit.MILLISECONDS

fun ConditionFactory.untilAssertedOrTimeout(timeoutInMillis: Long = 4000, assertion: () -> Unit) =
    this.atMost(timeoutInMillis, MILLISECONDS)
        .pollInSameThread()
        .ignoreExceptions()
        .untilAsserted { assertion() }