package com.joocy.fragments

import android.content.Intent
import android.support.v7.app.AppCompatActivity

abstract class PokemonSelectionHandler(val parentActivity: AppCompatActivity) {
    abstract fun onSelection(monster: PokemonRepository.Pokemon)
}

class SingleViewPokemonClickHandler(parentActivity: AppCompatActivity): PokemonSelectionHandler(parentActivity) {
    override fun onSelection(monster: PokemonRepository.Pokemon) {
        val intent = Intent(parentActivity, PokemonDetailActivity::class.java).apply {
            putExtra(PokemonDetailFragment.ARG_MONSTER_NAME, monster.name)
        }
        parentActivity.startActivity(intent)
    }
}

class MultiViewPokemonClickHandler(parentActivity: AppCompatActivity): PokemonSelectionHandler(parentActivity) {
    override fun onSelection(monster: PokemonRepository.Pokemon) {
        PokemonDetailFragment.display(monster.name, R.id.pokemon_detail_container, parentActivity.supportFragmentManager)
    }
}