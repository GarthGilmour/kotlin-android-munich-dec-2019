package com.joocy.debts

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import com.joocy.debts.data.Debt
import com.joocy.debts.data.DebtDatabase
import com.joocy.debts.data.DebtRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class DebtViewModel(application: Application): AndroidViewModel(application) {

    private var parentJob = Job()
    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Main
    private val scope = CoroutineScope(coroutineContext)

    private val repository: DebtRepository
    val debts : LiveData<List<Debt>>

    init {
        val debtDao = DebtDatabase.getDatabase(application, scope).debtDAO()
        repository = DebtRepository(debtDao)
        debts = repository.debts
    }

    override fun onCleared() {
        super.onCleared()
        parentJob.cancel()
    }

    fun add(debt: Debt) = scope.launch(Dispatchers.IO) { repository.add(debt) }

}