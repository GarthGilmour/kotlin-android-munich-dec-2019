package com.joocy.clickattack

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ClickCountViewModelTest {
    @Rule
    @JvmField
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private val clickCountObserver: Observer<Int> = mock()
    private val viewModel = ClickCountViewModel()

    @Before
    fun setup() {
        viewModel.clickCount.observeForever(clickCountObserver)
    }

    @Test
    fun resetShouldSetClickCountToZero() {
        viewModel.reset()
        verify(clickCountObserver).onChanged(0)
    }

    @Test
    fun incrementShouldAddClick() {
        viewModel.reset()
        viewModel.incrementCount()
        verify(clickCountObserver).onChanged(1)
    }
}