package com.joocy.compose

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.Composable
import androidx.compose.unaryPlus
import androidx.ui.core.Text
import androidx.ui.core.dp
import androidx.ui.core.setContent
import androidx.ui.layout.*
import androidx.ui.res.imageResource
import androidx.ui.tooling.preview.Preview

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            NewsStory()
        }
    }
}

@Composable
fun NewsStory() {
    val image = +imageResource(R.drawable.ic_header)
    Column(modifier= Spacing(16.dp)) {
        Container(modifier = Height(180.dp) wraps Expanded) {
            DrawImage(image)
        }
        HeightSpacer(16.dp)
        Text("A day in Shark Fin Cove")
        Text("Davenport, California")
        Text("December 2019")
    }
}

@Preview
@Composable
fun DefaultPreview() {
    NewsStory()
}