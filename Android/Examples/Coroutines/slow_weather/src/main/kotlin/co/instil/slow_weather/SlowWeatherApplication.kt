package co.instil.slow_weather

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import java.net.URI
import java.text.DecimalFormat
import java.time.LocalDateTime
import kotlin.random.Random

@SpringBootApplication
class SlowWeatherApplication

fun main(args: Array<String>) {
	runApplication<SlowWeatherApplication>(*args)
}

@RestController
class WeatherController(private val weatherService: WeatherService) {

	@GetMapping
	fun getWeather(): ResponseEntity<Weather> {
		val wait = Random.nextLong(1_000, 5_000)
		Thread.sleep(wait)
		return ResponseEntity
				.created(URI("/"))
				.header("X-Wait", wait.toString())
				.body(weatherService.getWeather())
	}

}

@Service
class WeatherService {

	val descriptions = arrayOf("Sun", "Showers", "Clouds", "Fog")

	fun getWeather() = Weather(generateDescription(), generateTemperature())

	private fun generateDescription() = descriptions.random()

	private fun generateTemperature(): Double {
		val hourNow = LocalDateTime.now().hour
		val temperature = when {
			hourNow.between(0, 9) -> Random.nextDouble(-2.0, 5.0)
			hourNow.between(10, 15) -> Random.nextDouble(6.0, 8.0)
			hourNow.between(16, 19) -> Random.nextDouble(7.0, 0.0)
			else -> Random.nextDouble(-5.0, 0.0)
		}
		return DecimalFormat("#.##").format(temperature).toDouble()
	}

}

data class Weather(val description: String, val temperature: Double)

fun Int.between(from: Int, to: Int) = this in from..to
