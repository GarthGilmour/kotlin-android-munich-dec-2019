package co.instil.roboelectric

import android.app.Activity.RESULT_OK
import android.app.Instrumentation
import android.content.Intent
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.Intents.intending
import androidx.test.espresso.intent.matcher.ComponentNameMatchers.hasShortClassName
import androidx.test.espresso.intent.matcher.IntentMatchers.anyIntent
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityTests {

    @get:Rule
    val rule = IntentsTestRule(MainActivity::class.java)

    @Test
    fun shouldZipTwoWords() {
        typeTwoWords()

        onView(withId(R.id.zipButton)).perform(click())

        onView(withId(R.id.resultView)).check(matches(withText("otnweo")))
    }

    @Test
    fun shouldReverseTwoWords() {
        typeTwoWords()

        onView(withId(R.id.reverseButton)).perform(click())

        onView(withId(R.id.resultView)).check(matches(withText("owteno")))
    }

    @Test
    fun shouldUpperTwoWords() {
        typeTwoWords()

        onView(withId(R.id.upperButton)).perform(click())

        onView(withId(R.id.resultView)).check(matches(withText("ONETWO")))
    }

    @Test
    fun shouldRetainStateBetweenActivityRestarts() {
        val subject = ActivityScenario.launch(MainActivity::class.java)
        typeTwoWords()

        subject.recreate()

        onView(withId(R.id.firstWordText)).check(matches(withText("one")))
        onView(withId(R.id.secondWordText)).check(matches(withText("two")))
    }

    @Test
    fun shouldUseWordFetchedFromFetchWordActivity() {
        val resultData = Intent().apply {
            putExtra(MainActivity.WORD_EXTRA, "testing")
        }
        val result = Instrumentation.ActivityResult(RESULT_OK, resultData)

        intending(anyIntent()).respondWith(result)

        onView(withId(R.id.fetchButton)).perform(click())

        onView(withId(R.id.firstWordText)).check(matches(withText("testing")))
    }

    @Test
    fun shouldStartTheAboutActivity() {
        onView(withId(R.id.aboutButton)).perform(click())

        intended(hasComponent(hasShortClassName(".AboutActivity")))
    }

    private fun typeTwoWords(wordOne: String = "one", wordTwo: String = "two") {
        onView(withId(R.id.firstWordText)).apply {
            perform(clearText())
            perform(typeText(wordOne))
        }

        onView(withId(R.id.secondWordText)).apply {
            perform(clearText())
            perform(typeText(wordTwo))
        }
    }

}