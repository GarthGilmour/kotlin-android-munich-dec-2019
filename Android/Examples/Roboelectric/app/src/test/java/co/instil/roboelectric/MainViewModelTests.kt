package co.instil.roboelectric

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class MainViewModelTests {

    @get:Rule
    val instantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    val subject = MainViewModel()

    @Before
    fun setup() {
        subject.firstWord.value = "one"
        subject.secondWord.value = "two"
    }

    @Test
    fun shouldZipTwoWords() {
        subject.zip()

        assertThat(subject.result.value, equalTo("otnweo"))
    }

    @Test
    fun shouldReverseTwoWords() {
        subject.reverse()

        assertThat(subject.result.value, equalTo("owteno"))
    }

    @Test
    fun shouldUpperTwoWords() {
        subject.upper()

        assertThat(subject.result.value, equalTo("ONETWO"))
    }

}