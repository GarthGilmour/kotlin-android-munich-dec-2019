package co.instil.roboelectric

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.natpryce.hamkrest.assertion.assertThat
import com.natpryce.hamkrest.equalTo
import kotlinx.android.synthetic.main.activity_main.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.Shadows.shadowOf
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O])
class MainActivityRobolectricTests {

    lateinit var subject: MainActivity

    @Before
    fun setup() {
        subject = Robolectric.setupActivity(MainActivity::class.java)
    }

    @Test
    fun shouldZipTwoWords() {
        subject.findViewById<EditText>(R.id.firstWordText).setText("one")
        subject.findViewById<EditText>(R.id.secondWordText).setText("two")

        subject.findViewById<Button>(R.id.zipButton).performClick()

        assertThat(subject.findViewById<TextView>(R.id.resultView).text.toString(), equalTo("otnweo"))
    }

    @Test
    fun shouldReverseTwoWords() {
        subject.findViewById<EditText>(R.id.firstWordText).setText("one")
        subject.findViewById<EditText>(R.id.secondWordText).setText("two")

        subject.findViewById<Button>(R.id.reverseButton).performClick()

        assertThat(subject.findViewById<TextView>(R.id.resultView).text.toString(), equalTo("owteno"))
    }

    @Test
    fun shouldUpperTwoWords() {
        subject.findViewById<EditText>(R.id.firstWordText).setText("one")
        subject.findViewById<EditText>(R.id.secondWordText).setText("two")

        subject.findViewById<Button>(R.id.upperButton).performClick()

        assertThat(subject.findViewById<TextView>(R.id.resultView).text.toString(), equalTo("ONETWO"))
    }

    @Test
    fun shouldLoadAboutActivity() {
        subject.findViewById<Button>(R.id.aboutButton).performClick()

        val expectedIntent = Intent(subject, AboutActivity::class.java)
        val actualIntent = shadowOf(RuntimeEnvironment.application).getNextStartedActivity()

        assertThat(actualIntent.component, equalTo(expectedIntent.component))
    }

    @Test
    fun shouldFetchWordFromFetchWordActivity() {
        subject.findViewById<Button>(R.id.fetchButton).performClick()

        val triggerIntent = Intent(subject, WordFetchActivity::class.java)
        val result = Intent().apply {
            putExtra(MainActivity.WORD_EXTRA, "testing")
        }
        shadowOf(subject).receiveResult(triggerIntent, Activity.RESULT_OK, result)

        assertThat(subject.firstWordText.text.toString(), equalTo("testing"))
    }

}