package co.instil.roboelectric

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import co.instil.roboelectric.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        val FETCH_WORD_REQUEST = 1001
        val WORD_EXTRA = "word"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val viewModel = ViewModelProviders
            .of(this)
            .get(MainViewModel::class.java)

        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.lifecycleOwner = this
        binding.vm = viewModel

        fetchButton.setOnClickListener {
            val intent = Intent(this, WordFetchActivity::class.java)
            startActivityForResult(intent, FETCH_WORD_REQUEST)
        }

        aboutButton.setOnClickListener {
            startActivity(Intent(this, AboutActivity::class.java))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == FETCH_WORD_REQUEST) {
            if (resultCode  == Activity.RESULT_OK) {
                val word = data?.getStringExtra(WORD_EXTRA)
                if (word != null) {
                    firstWordText.setText(word)
                }
            }
        }
    }
}

class MainViewModel: ViewModel() {

    val firstWord = MutableLiveData<String>("first")
    val secondWord = MutableLiveData<String>("second")
    val result = MutableLiveData<String>("result here")

    fun zip() {
        doWithFirstAndSecond { first, second ->
            first.zip(second).joinToString("") { pair -> "${pair.first}${pair.second}" }
        }
    }

    fun reverse() {
        doWithFirstAndSecond { first, second ->
            (first + second).reversed()
        }
    }

    fun upper() {
        doWithFirstAndSecond { first, second ->
            (first + second).toUpperCase()
        }
    }

    private fun doWithFirstAndSecond(cb: (String, String) -> String) {
        val _firstWord: String? = firstWord.value
        val _secondWord: String? = secondWord.value

        if (_firstWord != null && _secondWord != null) {
            val _result = cb(_firstWord, _secondWord)
            result.value = _result
        }
    }

}