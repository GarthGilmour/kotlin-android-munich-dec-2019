package co.instil.roboelectric

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_word_fetch.*

class WordFetchActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_word_fetch)

        okButton.setOnClickListener {
            val word = wordText.text.toString()
            val intent = Intent().apply {
                putExtra(MainActivity.WORD_EXTRA, word)
            }
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }
}
