package com.joocy.clickattack

import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import org.junit.Rule
import org.junit.Test

@LargeTest
class MainActivityTest {

    @Rule
    @JvmField
    var rule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun clickingOnSplatIncrementsCount() {
    }

    @Test
    fun clickOnResetDisplaysZero() {
    }

}