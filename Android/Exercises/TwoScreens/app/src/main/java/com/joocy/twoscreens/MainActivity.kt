package com.joocy.twoscreens

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            val intent = Intent(this, InfoActivity::class.java)
            startActivityForResult(intent, InfoActivity.INFO_REQUEST_CODE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == InfoActivity.INFO_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            data?.run {
                label.text = "${getStringExtra(InfoActivity.EXTRA_NAME)}, I CHOOSE YOU!!" }
        }
    }

}
