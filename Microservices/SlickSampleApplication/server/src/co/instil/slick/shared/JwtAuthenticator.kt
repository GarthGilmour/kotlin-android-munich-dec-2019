package co.instil.slick.shared

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm

const val secret = "GotoAmsterdam2019"

class SimpleJWT {
    private val algorithm = Algorithm.HMAC256(secret)

    val verifier: JWTVerifier = JWT.require(algorithm).build()

    fun sign(user: User): String = JWT
            .create()
            .withClaim("username", user.name)
            .withClaim("id", user.id.toString())
            .sign(algorithm)
}

val jwtService  = SimpleJWT()