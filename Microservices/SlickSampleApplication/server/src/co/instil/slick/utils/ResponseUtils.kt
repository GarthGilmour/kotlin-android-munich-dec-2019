package co.instil.slick.utils

import io.ktor.application.ApplicationCall
import io.ktor.http.ContentType
import io.ktor.response.respondText

suspend fun ApplicationCall.respondOk() {
    this.respondText("OK", contentType = ContentType.Text.Plain)
}
