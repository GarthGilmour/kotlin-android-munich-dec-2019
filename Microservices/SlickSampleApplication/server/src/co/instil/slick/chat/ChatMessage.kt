package co.instil.slick.chat


@Suppress("unused")
sealed class ChatMessage(val id: String) {
    class AllIMs(val messages: Iterable<ChatIM>) : ChatMessage("ALL_IMS")
    class NewIM(val message: ChatIM) : ChatMessage("NEW_IM")
    class UserCount(val count: Int) : ChatMessage("USER_COUNT")
}