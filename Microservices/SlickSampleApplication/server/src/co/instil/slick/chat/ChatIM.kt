package co.instil.slick.chat

import co.instil.slick.shared.User
import java.util.*

data class ChatIM(
    val user: User,
    val content: String,
    val timestamp: Date,
    val id: UUID = UUID.randomUUID()
)