package co.instil.slick.chat

import co.instil.slick.shared.User
import co.instil.slick.room.Room
import co.instil.slick.utils.Subject
import java.util.*

@Suppress("unused")
class ChatServiceInMemory : Subject<NewChatMessage>(), ChatService {
    override fun close() {}

    private val chats = Collections.synchronizedMap(
            mutableMapOf<Room, MutableList<ChatIM>>())

    override suspend fun getChat(room: Room): MutableList<ChatIM> {
        return getInMemoryChat(room)
    }

    private fun getInMemoryChat(room: Room) =
            chats.getOrPut(room, { Collections.synchronizedList(mutableListOf<ChatIM>()) })

    override suspend fun addMessage(room: Room, user: User, content: String) {
        val chatMessage = ChatIM(user, content, Date())
        val chat = getInMemoryChat(room)
        chat.add(chatMessage)

        invokeCallbacks(NewChatMessage(room, chatMessage))
    }
}