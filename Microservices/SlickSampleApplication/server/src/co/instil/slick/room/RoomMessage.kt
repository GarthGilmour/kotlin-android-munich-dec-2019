package co.instil.slick.room


@Suppress("unused")
sealed class RoomMessage(val id: String) {
    class AllRooms(val rooms: Iterable<Room>) : RoomMessage("ALL_ROOMS")
    class RoomUpdate(val update: co.instil.slick.room.RoomUpdate) : RoomMessage("ROOM_UPDATE")
}