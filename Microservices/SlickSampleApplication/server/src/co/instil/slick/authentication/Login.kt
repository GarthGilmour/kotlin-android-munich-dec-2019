package co.instil.slick.authentication

data class Login(val username: String, val password: String)