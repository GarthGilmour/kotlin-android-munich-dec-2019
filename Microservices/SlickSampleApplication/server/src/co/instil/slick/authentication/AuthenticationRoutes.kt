package co.instil.slick.authentication

import co.instil.slick.shared.SlickUserPrincipal
import co.instil.slick.shared.User
import co.instil.slick.shared.jwtService
import co.instil.slick.utils.respondOk
import io.ktor.application.call
import io.ktor.auth.authenticate
import io.ktor.auth.principal
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.*
import org.slf4j.LoggerFactory

private val logger = LoggerFactory.getLogger("co.instil.slick.Authentication")

fun Route.authenticationRoutes() {
    post("login") {
        val login = call.receive<Login>()
        logger.debug("Logging in ${login.username}")

        val user = userService.getByName(login.username)
        if (user == null || user.password != login.password) {
            call.respond(HttpStatusCode.NoContent)
        } else {
            // INTERESTING - Creating JSON Response objects on the fly
            call.respond(
                mapOf(
                    "token" to jwtService.sign(user),
                    "user" to User(login.username, "", user.id)
                )
            )
        }
    }

    post("register") {
        // INTERESTING - Getting application body from call
        val login = call.receive<Login>()
        userService.add(User(login.username, login.password))
        call.respondOk()
    }

    authenticate {
        get {
            val principal = call.principal<SlickUserPrincipal>()
            if (principal == null) {
                call.respond(HttpStatusCode.NoContent)
                return@get
            }

            call.respond(User(principal.username, "", principal.id))
        }

        get("allusers") {
            val response = userService.getAll()
                .map { User(it.name, "", it.id) }
            call.respond(response)
        }
    }
}