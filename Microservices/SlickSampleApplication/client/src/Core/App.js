import React from 'react';
import {connect} from "react-redux";

import styles from "./App.module.scss";
import Login from "../User/Login";
import Home from "./Home";

const App = ({currentUser, isInitialised}) => (
    <div className={styles.wrapper}>
        {!isInitialised
            ? <h1>Initialising....</h1>
            : currentUser === null
                ? <div className={styles.center}>
                    <div className={styles.title}>slic<img src='/assets/KotlinK_tiny.png'
                                                           alt='k'/>
                    </div>
                    <Login/>
                </div>
                : <Home/>
        }
    </div>
);

const mapStateToProps = (state) => ({
    isInitialised: state.user.isInitialised,
    currentUser: state.user.currentUser
});

const mapDisaptchToProps = ({});

export default connect(mapStateToProps, mapDisaptchToProps)(App);
