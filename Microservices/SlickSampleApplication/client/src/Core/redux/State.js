
export const initialState = {
    rooms: [],
    currentRoom: null,
    connectionFails: 0,
    connectingToServer: true,
    serverFailure: false
};
