import React from 'react';

import styles from './Home.module.scss';
import {connect} from "react-redux";
import Chat from "../Chat/Chat";
import {logoutUser} from "../User/redux/Actions";
import Rooms from "./Room";

const Connecting = React.memo(() => (
    <div>
        <h1>Connecting to server...</h1>
    </div>
));

const ServerFailure = React.memo(() => (
    <div>
        <h1>Failure in connecting to server</h1>
        <h2>Please retry later</h2>
    </div>
));

const Sidebar = React.memo(({logout, username}) => (
    <div className={styles.sidebar}>
        <div className='mb-2 row justify-content-center'>
            <div className='col align-self-start'>{username}</div>
            <div className='col align-self-end text-right'>
                <button className='btn btn-danger p-1' onClick={logout}>Logout</button>
            </div>
        </div>
        <hr className={styles.separator}/>
        <span><img className={styles.logo} src='/assets/logo.png' alt='slicK'/></span>
        <Rooms/>
    </div>
));

const Home = ({serverFailure, connectingToServer, logout, username}) => (
    <div className={styles.wrapper}>
        <Sidebar logout={logout} username={username}/>
        <div className={styles.content}>
            {serverFailure
                ? <ServerFailure/>
                : connectingToServer
                    ? <Connecting/>
                    : <Chat/>
            }
        </div>
    </div>
);

const mapStateToProps = (state) => ({
    serverFailure: state.core.serverFailure,
    connectingToServer: state.core.connectingToServer,
    username: state.user.currentUser.name
});

const mapDisaptchToProps = ({
    logout: logoutUser
});

export default connect(mapStateToProps, mapDisaptchToProps)(Home);
