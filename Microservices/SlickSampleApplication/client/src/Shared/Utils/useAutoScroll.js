import {useRef, useEffect} from 'react';

// INTERESTING - Custom React Hook
export function useAutoScrollToBottom() {
    const ref = useRef(null);

    useEffect(() => {
        if (ref.current) {
            ref.current.scrollTo(0, Number.MAX_SAFE_INTEGER);
        }
    });

    return ref;
}
