// INTERESTING - Helpers for creating actions creators
//               Creators have embedded ids for use in Reducer
export const createAction = (type) => {
    let creator = (payload) => {
        return ({
            type,
            payload
        });
    };

    creator.id = type;
    return creator;
};

// Slightly more efficient as uses a single action object
export const createEmptyAction = (type) => {
    const action = {type};

    let creator = () => action;
    creator.id = type;

    return creator;
};

export const createAsyncAction = (typePrefix) => ({
    request: createAction(`${typePrefix}/REQUEST`),
    success: createAction(`${typePrefix}/SUCCESS`),
    failure: createAction(`${typePrefix}/FAILURE`)
});
