
export const initialState = {
    currentUser: null,
    login: {
        username: '',
        password: ''
    },
    isLoggingIn: false,
    isInitialised: false
};
