
const tokenId = 'userToken';

export class JwtService {
    static get() {
        return localStorage.getItem(tokenId);
    }

    static set(token) {
        localStorage.setItem(tokenId, token);
    }

    static clear() {
        localStorage.removeItem(tokenId);
    }
}
