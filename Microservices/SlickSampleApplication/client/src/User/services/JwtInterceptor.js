import axios from 'axios';
import {JwtService} from "./JwtService";

axios.interceptors.request.use((config) => {
    const token = JwtService.get();
    if (token) {
        config.headers = {
            ...config.headers,
            "Authorization": `Bearer ${token}`,
        };
    }
    return config;
});
