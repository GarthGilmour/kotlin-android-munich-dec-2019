import {initialState} from "./State";
import {
    chatLoad,
    newIM,
    sendNewIM,
    setChatSocket,
    setRoomName,
    currentMessageUpdate,
    setRoomUserCount,
    changingRoom
} from "./Actions";

export const chatReducer = (state = initialState, action) => {
    switch (action.type) {
        case setRoomName.id:
            return {
                ...state,
                changingRoom: false,
                name: action.payload
            };
        case changingRoom.id:
            return {
                ...state,
                changingRoom: true
            };
        case setChatSocket.id:
            return {
                ...state,
                socket: action.payload,
                messages: []
            };
        case chatLoad.id:
            return {
                ...state,
                messages: action.payload
            };
        case newIM.id:
            return {
                ...state,
                messages: [...state.messages, action.payload]
            };
        case sendNewIM.request.id:
            return {
                ...state,
                isSending: true
            };
        case sendNewIM.success.id:
            return {
                ...state,
                isSending: false,
                currentMessages: {
                    ...state.currentMessages,
                    [action.payload]: ''
                }
            };
        case sendNewIM.failure.id:
            return {
                ...state,
                isSending: false,
            };
        case currentMessageUpdate.id:
            return {
                ...state,
                currentMessages: {
                    ...state.currentMessages,
                    [state.name]: action.payload
                }
            };
        case setRoomUserCount.id:
            return {
                ...state,
                userCount: action.payload
            };
        default:
            return state;
    }
};
