import React from 'react';
import {connect} from "react-redux";
import {currentMessageUpdate, sendIMAsync} from "./redux/Actions";

import styles from './Chat.module.css';
import {useAutoScrollToBottom} from "../Shared/Utils/useAutoScroll";

const Messages = React.memo(({messages, currentUser}) => {
    const messageRef = useAutoScrollToBottom();

    return (
        <div className={`${styles.chatMessages}`} ref={messageRef}>
            {messages.map(message =>
                <div key={message.id}
                     className={currentUser.name === message.user.name ? styles.myMessage : styles.message}>
                    <div className={styles.timestamp}>{message.user.name} <span
                        className={styles.timestamp}>{message.timestamp.toLocaleString()}</span></div>
                    <div>{message.content}</div>
                </div>)
            }
        </div>
    );
});

const ChatInput = ({message, send, room, update, disabled}) => (
    <div className='row'>
            <textarea className={`${styles.currentMessage} col`} disabled={disabled}
                      value={message} onChange={e => update(e.target.value)}/>
        <span className='col-auto'>
            <button className={`btn btn-primary ${styles.send}`} onClick={() => send(room, message)}
                    disabled={message === '' || disabled}>Send
            </button>
        </span>
    </div>
);

const Chat = ({name, messages, currentMessageUpdate, message, sendIM, isSending, currentUser, userCount}) => {
    return !name
        ? <h2>Select a room to begin</h2>
        : (
            <div className={styles.chatContainer}>
                <h2>{name} Messages - {userCount} viewing room</h2>
                <Messages messages={messages} currentUser={currentUser}/>
                <div className='mt-2 ml-2'>
                    <ChatInput update={currentMessageUpdate} message={message}
                               send={sendIM} room={name}
                               disabled={isSending}/>
                </div>
            </div>
        );

};

const mapStateToProps = (state) => ({
    name: state.chat.name,
    messages: state.chat.messages,
    message: state.chat.currentMessages[state.chat.name] || '',
    isSending: state.chat.isSending,
    currentUser: state.user.currentUser,
    userCount: state.chat.userCount
});

const mapDisaptchToProps = ({
    currentMessageUpdate,
    sendIM: sendIMAsync
});

export default connect(mapStateToProps, mapDisaptchToProps)(Chat);
