
export function remapIM(message) {
    return {
        ...message,
        timestamp: new Date(message.timestamp)
    }
}

export function remapIMs(messages) {
    return messages.map(remapIM);
}
