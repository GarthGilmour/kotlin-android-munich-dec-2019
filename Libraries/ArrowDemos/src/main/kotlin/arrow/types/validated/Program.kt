package arrow.types.validated

import arrow.data.*
import arrow.data.extensions.validated.applicative.applicative
import arrow.typeclasses.Semigroup

class Employee(val id: String, val age: Int, val dept: String) {
    override fun toString() = "$id of age $age working in $dept"
}

fun askQuestion(question: String): String {
    println(question)
    return readLine() ?: ""
}

fun checkID(): Validated<String, String> {
    val regex = Regex("[A-Z]{2}[0-9]{2}")
    val response = askQuestion("Whats your ID?")
    return if(regex.matches(response)) Valid(response) else Invalid("Bad ID")
}

fun checkAge(): Validated<String, Int> {
    val response = askQuestion("How old are you?").toInt()
    return if(response > 16) Valid(response) else Invalid("Bad Age")
}

fun checkDept(): Validated<String, String> {
    val depts = listOf("HR", "Sales", "IT")
    val response = askQuestion("Where do you work?")
    return if(depts.contains(response)) Valid(response) else Invalid("Bad Dept")
}

fun main() {
    val sg = object : Semigroup<String> {
        override fun String.combine(b: String) = "$this $b"
    }
    val id = checkID()
    val age = checkAge()
    val dept = checkDept()
    val result = Validated.applicative(sg)
                          .map(id, age, dept) { (a,b,c) -> Employee(a,b,c) }
            .fix()
    println(result.fold({ "Error: $it" }, {"Result: $it"} ))
}
