package basics.with

class Turtle {
    fun turnRight(degrees: Int = 90) = println("Turtle turning right")
    fun turnLeft(degrees: Int = 90) = println("Turtle turning left")
    fun forward(distance: Int = 10) = println("Turtle moving forward")
    fun back(distance: Int = 10) = println("Turtle moving back")
    fun penUp() = println("Turtle pen up")
    fun penDown() = println("Turtle pen down")
}

fun main() {
    val turtle = Turtle()
    with (turtle) {
        forward(100)
        turnRight()
        penDown()
        forward(50)
        penUp()
    }
}
